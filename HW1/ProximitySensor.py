'''
@file ProximitySensor.py

This file implements a ProximitySensor class for controlling an elevator

This file was adapted from an example from the lecture

It implements a virtual proximity sensor that "senses" floors for an elevator
'''
from random import choice

class ProximitySensor:
    '''
    @brief      A ProximitySensor class
    @details    This class represents a ProximitySensor that will activate
                based on how close it is to an object. As of right now this
                class is implemented using "pseudo-hardware". That is, we are
                not working with real hardware IO yet, this is all pretend.  
    '''

    ## Constant representing the On state of the sensor
    ON = 1

    ## Constant representing the Off state of the sensor
    OFF = 0
    def __init__(self, pin):
        '''
        @brief      Creates a ProximitySensor object
        @param pin  A pin object that the ProximitySensor is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('ProximitySensor object created attached to pin '+ str(self.pin))

    
    def getSensorState(self):
        '''
        @brief      Gets the sensor state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button. True for
                    close to an object, false for not.
        '''
        return choice([self.ON, self.OFF])


