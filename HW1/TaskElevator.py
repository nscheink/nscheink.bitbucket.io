'''
@file TaskElevator.py

This file implements a finite-state-machine class for controlling an elevator

This file was adapted from an example from the lecture

It implements a virtual finite-state-machine in Python for controlling an 
elevator between two floors.

The user has two buttons that indicate intent to go to first and second floors

There are also two proximity sensors for each floor
'''

import time
from Button import *
from MotorDriver import *
from ProximitySensor import *

## A Finite-State-Machine representing an Elevator
#
# Has 6 states:\n 
# INIT              - Initialize, clear the buttons, and start moving down\n 
# MOVING_DOWN       - Use motor to move down and wait for proximity sensor\n 
# STOPPED_AT_FIRST  - Wait for a button push to start moving up\n 
# MOVING_UP         - Use motor to move up and wait for proximity sensor\n
# DO_NOTHING        - Just chill\n
class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    INIT                = 0
    
    ## Constant defining State 1 - Moving down from second floor to first
    MOVING_DOWN         = 1
    
    ## Constant defining State 2 - Stopped at the first floor
    STOPPED_AT_FIRST    = 2
    
    ## Constant defining State 3 - Moving up from first floor to second
    MOVING_UP           = 3
    
    ## Constant defining State 4 - Stopped at the second floor
    STOPPED_AT_SECOND   = 4
    
    ## Constant defining State 5 - Do nothing, Relax, Chill
    DO_NOTHING          = 5

    
    ## brief Creates a TaskWindshield object.
    # @param interval A double representing the loop period
    # @param Motor A MotorDriver object that the elevator controls
    # @param FirstButton A Button representing the first floor button
    # @param SecondButton A Button representing the second floor button
    # @param FirstSensor A ProximitySensor sensing when first floor reached
    # @param SecondSensor A ProximitySensor sensing when second floor reached
    
    def __init__(   
                    self, 
                    interval, 
                    Motor, 
                    FirstButton, 
                    SecondButton, 
                    FirstSensor, 
                    SecondSensor
                ):

       
        ## The state to run on the next iteration of the task
        self.state = self.INIT
        
        ## An object instance reference of the motor object
        self.Motor = Motor
                         
        
        ## An object instance reference of the first floor button
        self.FirstButton = FirstButton

        ## An object instance reference of the second floor button
        self.SecondButton = SecondButton

        ## An object instance reference of the first floor sensor
        self.FirstSensor = FirstSensor

        ## An object instance reference of the second floor sensor
        self.SecondSensor = SecondSensor
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval

        ## The "timestamp" of the currently running task
        self.curr_time = self.start_time

    
    def run_init(self):
        '''
        @brief Runs the initialization code
        @details Transitions the elevator towards moving down
        '''
        self.FirstButton.clear()
        self.SecondButton.clear()
        self.transitionTo(self.MOVING_DOWN)
        self.Motor.Down()

    def run_stopped_at_first(self):
        '''
        @brief Runs the stopped at first code 
        @details Clears the first floor button and waits for the second floor
        button to transition towards moving up
        '''
        self.FirstButton.clear() # 1st floor button does nothing on 1st floor
        if self.SecondButton.getButtonState() == Button.ON:
            self.transitionTo(self.MOVING_UP)
            self.Motor.Up()

    def run_stopped_at_second(self):
        '''
        @brief Runs the stopped at second code
        @details Clears the second floor button and waits for the first floor
        button to transition towards moving down
        '''
        self.SecondButton.clear() # 2nd floor button does nothing on 2nd floor
        if self.FirstButton.getButtonState() == Button.ON:
            self.transitionTo(self.MOVING_DOWN)
            self.Motor.Down()
 
    def run_moving_up(self):
        '''
        @brief Runs the moving up code 
        @details Waits for the second floor proximity sensor to report on
        before transitioning towards stopping at the second floor
        '''
        if self.SecondSensor.getSensorState() == ProximitySensor.ON:
            self.SecondButton.clear()
            self.transitionTo(self.STOPPED_AT_SECOND)
            self.Motor.Stop()

    def run_moving_down(self):
        '''
        @brief Runs the moving down code 
        @details Waits for the first floor proximity sensor to report on before 
        transitioning towards stopping at the first floor
        '''
        if self.FirstSensor.getSensorState() == ProximitySensor.ON:
            self.FirstButton.clear()
            self.transitionTo(self.STOPPED_AT_FIRST)
            self.Motor.Stop()

    def run_current_state(self):
        '''
        @brief Run the code for the state the elevator is currently in
        '''
        print(str(self.runs) + ' State {:d} {:0.2f}'.format(self.state, 
            self.curr_time - self.start_time))
        if(self.state == self.INIT):
            self.run_init()

        elif(self.state == self.MOVING_DOWN):
            # Run State 1 Code
            self.run_moving_down()
        elif(self.state == self.STOPPED_AT_FIRST):
            # Run State 2 Code
            self.run_stopped_at_first()
        elif(self.state == self.MOVING_UP):
            # Run State 3 Code
            self.run_moving_up()
        elif(self.state == self.STOPPED_AT_SECOND):
            # Run State 4 Code
            self.run_stopped_at_second()
        else:
            # Uh-oh state (undefined sate)
            # Error handling
            print("Bad state!")
            pass

    def run(self):
        '''
        @brief Runs one iteration of the task 
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            self.run_current_state() 
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

