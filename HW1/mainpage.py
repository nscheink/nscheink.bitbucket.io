## @file mainpage.py
#  Stores the project information 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Creates a virtual finite state machine
#
#  @section sec_elev Elevator
#  The elevator finite state machine has two buttons, two floors, and one motor that it controls. It is based off the following finite state machine diagram:
#  \image html fsm.png
#  Please see TaskElevator.TaskElevator for more details.
#
#  @section sec_mot Motor Driver
#  Creates a virtual motor for the elevator to control. Please see MotorDriver.MotorDriver for more details.
#
#  @section sec_but Button
#  Creates a virtual button for the elevator to read and control. Please see Button.Button for more details.
#
#  @section sec_psens Proximity Sensor
#  Creates a virtual proximity sensor for the elevator to read from. Please see ProximitySensor.ProximitySensor for more details.
#
#  @author Nathan Scheinkman
#
#  @copyright License Info
#
#  @date October 7, 2020
#
