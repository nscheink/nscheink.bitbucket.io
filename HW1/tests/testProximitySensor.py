import sys
if '..' not in sys.path:
    sys.path.append('..')

import unittest

from ProximitySensor import ProximitySensor

class TestProximitySensor(unittest.TestCase):
    def test_init(self):
        ps = ProximitySensor('PB8')
        self.assertEqual(ps.pin, 'PB8')
    
    def test_state(self):
        ps = ProximitySensor('PB8')
        for n in range(100):
            state = ps.getSensorState()
            self.assertTrue( (state == ProximitySensor.ON) 
                            or (state == ProximitySensor.OFF) ) 

if __name__ == '__main__':
    unittest.main()
