''' @file main.py
Contains the fibonacci generator and user interface
'''

def fib(idx):
    ''' This method calculates a Fibonacci number corresponding to a specified
    index.
    @param idx An integer specifying the index of the desired Fibonacci number.
    '''

    # Edge cases
    if(idx == 0):
        return 0
    if(idx == 1):
        return 1
 
    # O(n) implementation - takes advantage of python having arbitrary integer
    # sizes for large numbers
    fa = 0
    fb = 1
    while(idx > 1):
        fib_idx = fa + fb
        fa = fb
        fb = fib_idx
        idx-=1

    return fib_idx

def run_ui():
    ''' This method runs the user interface to specify fibonacci input
    @return A boolean specifying whether to continue (True) or exit (False)
    '''

    # String constants displayed to or used by user
    exit_command = "exit"
    prompt_string = "Please input the index for a desired Fibonacci number. "\
    "Type "+exit_command+" to exit the program." 
    prompt_look = "> "
    int_conversion_error = "ERROR: Please input a integer."
    negative_int_error = "ERROR: Please input a positive number (n>=0)."

    # Show the user the prompt
    print(prompt_string)

    # Grab user input, exiting on EOF or exit command
    try:
        user_input = input(prompt_look)
    except EOFError as error:
        print("Exiting")
        return False
    if(user_input == exit_command):
        print("Exiting")
        return False

    # Check user input for positive integer input
    try: 
        idx = int(user_input)
    except Exception as ex:
        print(int_conversion_error)
        return True
    if(idx < 0):
        print(negative_int_error)
        return True
    
    # Run fibonacci generator on user index
    fib_idx = fib(idx)

    # Print output
    print("The Fibonacci number at index "+str(idx)+" is: "+str(fib_idx)+"\n")

    # Specify to continue running the program
    return True


if __name__ == '__main__':
    
    # Constantly run the user interface while it returns True
    while(run_ui()):
        pass
