## @file mainpage.py
#  Documentation mainpage code
#
#  Stores all the information for the mainpage
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Runs a fibonacci generator with a command line user interface. Uses an O(n) algorithm to generate fibonacci numbers of arbitrary size. Please see \ref main.py for more details.
#
#  @section sec_tests Unit Tests
#  This program ensures a proper implementation of the fibonacci generator within main by testing it against known values. Please see \ref unit_tests.py for more details.
#
#  @section sec_src Source Code
#  The source code for this program can be found on bitbucket at the link [here](https://bitbucket.org/nscheink/me305labs/src/master/Lab1/).
#  @author Nathan Scheinkman
#
#  @copyright License Info
#
#  @date September 22, 2020
#
