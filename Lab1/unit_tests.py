''' @file unit_tests.py
This runs tests separate from main.py to ensure proper functionality of the methods within main.py

Uses python's default unittest framework to run tests
'''
import unittest
import main

class TestFibonacci(unittest.TestCase):
    '''Wrapper for all the unit test methods'''

    def test_fib0(self):
        '''Tests that the 0'th term is correct'''
        self.assertEqual(main.fib(0), 0)

    def test_fib1(self):
        '''Tests that the 1st term is correct'''
        self.assertEqual(main.fib(1), 1)

    def test_fib2(self):
        '''Tests that the 2nd term is correct'''
        self.assertEqual(main.fib(2), 1)
    
    def test_fib3(self):
        '''Tests that the 3rd term is correct'''
        self.assertEqual(main.fib(3), 2)

    def test_fib4(self):
       '''Tests that the 4th term is correct'''
       self.assertEqual(main.fib(4), 3)

    def test_fib5(self):
        '''Tests that the 5th term is correct'''
        self.assertEqual(main.fib(5), 5)

    def test_fib6(self):
        '''Tests that the 6th term is correct'''
        self.assertEqual(main.fib(6), 8)

    def test_fib20(self):
        '''Tests that the 20th term is correct'''
        self.assertEqual(main.fib(20), 6765)

    def test_fib1999(self):
        '''Tests that the 1999th term is correct'''
        self.assertEqual(main.fib(1999),
2611005926183501768338670946829097324475555189114843467397273230483773870037923307730410719313972291638157639230613843870597997481070930648667960025707364078851859017098672504986584144842548768373271309551281830431960537091677315014266625027123872238011234749984205478230617988978500613170516952885123444971471854671812569739975450866912490650853945622130138277040986146312325044424769652148982077548213909414076005501)

    def test_fib2000(self):
        '''Tests that the 2000th term is correct'''
        self.assertEqual(main.fib(2000), 4224696333392304878706725602341482782579852840250681098010280137314308584370130707224123599639141511088446087538909603607640194711643596029271983312598737326253555802606991585915229492453904998722256795316982874482472992263901833716778060607011615497886719879858311468870876264597369086722884023654422295243347964480139515349562972087652656069529806499841977448720155612802665404554171717881930324025204312082516817125)

if __name__ == '__main__':
    unittest.main()

