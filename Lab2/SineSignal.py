'''
@file SineSignal.py

Implements a signal representing a sine wave
'''

from FunctionSignalInterface import FunctionSignalInterface
from math import sin, pi

## Signal class representing a Sine wave
class SineSignal(FunctionSignalInterface):

    ## Returns the sine signal at time t
    # @param t float representing the time the signal is currently at.
    # @return A float between 0 and 1 representing the signal value.
    # The signal is normalized to have a period and frequency of 1.
    def valueAt(t: float) -> float:
        return 0.5*sin(2*pi*t)+0.5
