'''
@file TaskBlinker.py

Implements an LED blinker for use with MicroPython
'''
from FunctionSignalInterface import FunctionSignalInterface
import pyb
import time

class TaskBlinker:

    VIRTUAL = 'virtual' 
    PWM_FREQUENCY = 20000
    ## Initializes the TaskBlinker
    def __init__(self, pin, signal: FunctionSignalInterface , frequency: float):

        ## The pin the LED is connected to
        # Use TaskBlinker.VIRTUAL for a virtual pin
        if(pin == TaskBlinker.VIRTUAL):
            self.pin = pin
        else:
            pin_obj = pyb.Pin(pin)
            tim2 = pyb.Timer(2, freq=TaskBlinker.PWM_FREQUENCY)
            self.pin = tim2.channel(1, pyb.Timer.PWM, pin = pin_obj)

        ## The signal for the LED to emulate
        self.signal = signal

        ## The frequency in Hz for the LED to run the signal at
        self.frequency = frequency

        ## Sets the zero point for time
        self.start_time = time.ticks_ms()

        ## Keeps track of previous output values for edge detection
        self.previous_val = 0

    ## Runs the TaskBlinker
    def run(self):
        curr_time = (time.ticks_ms() - self.start_time)/1000
        signal_time = curr_time*self.frequency
        output_val = 100*self.signal.valueAt(signal_time)
        if(self.pin == TaskBlinker.VIRTUAL):
            if(output_val-self.previous_val > 0):
                print("VIRTUAL LED ON")
            if(output_val-self.previous_val < 0):
                print("VIRTUAL LED OFF")
        else:
            self.pin.pulse_width_percent(output_val)

        self.previous_val = output_val

    ## Changes the LED signal function to a new signal
    # @param newSignal the new signal function to use
    def change_signal_to(self, newSignal: FunctionSignalInterface):
        self.signal = newSignal

    ## Changes the LED signal frequency to a new frequency
    # @param frequency the frequency in Hz
    def change_frequency_to(self, frequency: float):
        self.frequency = frequency

