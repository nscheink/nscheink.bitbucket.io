var annotated_dup =
[
    [ "FunctionSignalInterface", null, [
      [ "FunctionSignalInterface", "classFunctionSignalInterface_1_1FunctionSignalInterface.html", "classFunctionSignalInterface_1_1FunctionSignalInterface" ]
    ] ],
    [ "SineSignal", null, [
      [ "SineSignal", "classSineSignal_1_1SineSignal.html", "classSineSignal_1_1SineSignal" ]
    ] ],
    [ "SquareSignal", null, [
      [ "SquareSignal", "classSquareSignal_1_1SquareSignal.html", "classSquareSignal_1_1SquareSignal" ]
    ] ],
    [ "TaskBlinker", null, [
      [ "TaskBlinker", "classTaskBlinker_1_1TaskBlinker.html", "classTaskBlinker_1_1TaskBlinker" ]
    ] ],
    [ "TriangleSignal", null, [
      [ "TriangleSignal", "classTriangleSignal_1_1TriangleSignal.html", "classTriangleSignal_1_1TriangleSignal" ]
    ] ]
];