

import pyb
import time
from TaskBlinker import TaskBlinker
from SquareSignal import SquareSignal
from TriangleSignal import TriangleSignal
from SineSignal import SineSignal

def main():
    virtual_blinker = TaskBlinker(TaskBlinker.VIRTUAL, SquareSignal, 1/2)
    led_blinker = TaskBlinker(pyb.Pin.cpu.A5, TriangleSignal, 1)

    start_time = time.time()
    signal_switch_time = 30 # seconds
    switched = False
    while True:
        virtual_blinker.run()
        led_blinker.run()

        # Switch the LED signal type every 'signal_switch_time' seconds
        curr_time = time.time() - start_time
        if(curr_time % signal_switch_time) == 0: 
            if(switched == False):
                switched = True
                if(led_blinker.signal == TriangleSignal):
                    print("Switching physical LED to sine signal")
                    led_blinker.change_signal_to(SineSignal)
                elif(led_blinker.signal == SineSignal):
                    print("Switching physical LED to triangle signal")
                    led_blinker.change_signal_to(TriangleSignal)
        else:
            switched = False

if __name__ == '__main__':
    main()
