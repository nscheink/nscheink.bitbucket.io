## @file mainpage.py
#  Stores the project information 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Sets up a blinking LED on a Nucleo microcontroller, as well as a virtual LED
#
#  @section sec_blinker TaskBlinker
#  The blinker has a pin, frequency, and signal type. It accepts a FunctionSignalInterface for the signal. Please see TaskBlinker.TaskBlinker for more details.
#
#  @section sec_interface FunctionSignalInterface
#  Signal interface that the TaskBlinker interacts with. Allows for new signal implementations to be created that the TaskBlinker does not need to know about. To conform to this interface, signals need to have a period of 1 and an output ranging from 0 to 1. This allows the Blinker to change the frequency and amplitude on its own. Please see FunctionSignalInterface.FunctionSignalInterface for more details.
#
#  @section sec_sine SineSignal
#  Creates a sine wave implementation of the FunctionSignalInterface. Please see SineSignal.SineSignal for more details.
#
#  @section sec_triangle TriangleSignal
#  Creates a triangle wave implementation of the FunctionSignalInterface. Please see TriangleSignal.TriangleSignal for more details.
#
#  @section sec_square SquareSignal
#  Creates a square wave implementation of the FunctionSignalInterface. Please see SquareSignal.SquareSignal for more details.
#
#  @author Nathan Scheinkman
#
#  @copyright License Info
#
#  @date October 7, 2020
#
