import sys
if '..' not in sys.path:
    sys.path.append("..")
import unittest

from SineSignal import SineSignal
import math

class TestSineSignal(unittest.TestCase):
    def test_0(self):
        self.assertAlmostEqual(SineSignal.valueAt(0), 0.5)

    def test_quarter(self):
        self.assertAlmostEqual(SineSignal.valueAt(0.25), 1)

    def test_half(self):
        self.assertAlmostEqual(SineSignal.valueAt(0.5), 0.5)

    def test_three_quarter(self):
        self.assertAlmostEqual(SineSignal.valueAt(0.75), 0)

    def test_1(self):
        self.assertAlmostEqual(SineSignal.valueAt(1), 0.5)



if __name__ == '__main__':
    unittest.main()
