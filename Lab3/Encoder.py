'''
@file Encoder.py

Implements a class representing a Motor Encoder
'''
import pyb

## Encoder class representing a Motor Encoder
class Encoder:

    ## Initializes the Encoder
    def __init__(self, timer_no, ch_A_pin, ch_A_no, ch_B_pin, ch_B_no):

        ## The board timer number for the encoder
        self.timer_no = timer_no

        ## The pin that Channel A is connected to
        self.ch_A_pin = ch_A_pin

        ## The timer number channel for Channel A
        self.ch_A_no = ch_A_no

        ## The pin that Channel B is connected to
        self.ch_B_pin = ch_B_pin

        ## The timer number channel for Channel B
        self.ch_B_no = ch_B_no

        ## The timer that counts the encoder position 
        self.timer = pyb.Timer(self.timer_no, prescaler=0, period=0xFFFF)

        ## Channel A timer channel
        self.chA = self.timer.channel(  self.ch_A_no, 
                                        mode=pyb.Timer.ENC_AB, 
                                        pin= self.ch_A_pin)

        ## Channel B timer channel
        self.chB = self.timer.channel(  self.ch_B_no, 
                                        mode=pyb.Timer.ENC_AB, 
                                        pin= self.ch_B_pin)

        ## The encoder's absolute position represented by counter ticks
        self.position = 0

        ## The encoder counter position, wraps around 0x0000 to 0xFFFF
        self.counter = 0

        ## The change in encoder counter position in the most recent cycle
        self.delta = 0

        ## The change in encoder's absolute position in the most recent cycle
        self.pos_delta = 0

    ## Calculates the position change from the new and old counter
    # @param c2 the new counter position
    # @param c1 the old counter position
    def create_pos_delta(self,c2,c1):
        # c2 and c1 are 16 bit integers
        # diff is arbitrary size
        diff = 0
        if(c2 > 0xC000 and c1 <= 0x4000):
            # Underflow
            diff = c2 - (0xFFFF + c1 + 1)
        elif(c1 > 0xC000 and c2 <= 0x4000):
            # Overflow
            diff = (0xFFFF + c2 + 1) - c1
        else:
            diff = c2 - c1
        return diff

    ## Updates the encoder properties, ran every cycle
    def update(self):
        newCounter = self.timer.counter()
        self.delta = newCounter - self.counter
        self.pos_delta = self.create_pos_delta(newCounter, self.counter)
        self.counter = newCounter
        self.position += self.pos_delta

    ## Retrieves the encoder's absolute position
    # @return The encoder's absolute position
    def get_position(self):
        return self.position

    ## Sets the encoder's absolute position
    # @param pos the new absolute position
    def set_position(self, pos):
        self.position = pos

    ## Gets the encoder's change in counter position
    # @return the encoder's change in counter position
    def get_delta(self):
        return self.delta

