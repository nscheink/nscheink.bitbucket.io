'''
@file UserInterface.py

Implements a UserInterface class to allow user interaction with the encoder
'''

import pyb

## UserInterface class for the Encoder
class UserInterface:

    ## Constant representing the help command character
    HELP_CHAR=b'?'

    ## Constant holding the character to zero the encoder position
    ZERO_CHAR=b'z'

    ## Constant holding the character to print the encoder position
    POS_CHAR=b'p'

    ## Constant holding the character to print the encoder delta
    DELTA_CHAR=b'd'

    ## Initializes the UserInterface with a UART stream running on ID 2
    def __init__(self, encoder):
        ## The encoder the UserInterface is controlling 
        self.encoder = encoder

        ## The serial stream to communicate over
        self.uart = pyb.UART(2)
        self.print_instructions()

    ## Write a string over the UserInterface serial output
    # @param input_str The UTF-8 string to write
    def write_string(self, input_str):
        self.uart.write(input_str.encode('utf-8'))

    ## Print the User Interface instructions
    def print_instructions(self):
        self.write_string("\n\r---COMMANDS---\n\r")
        self.write_string("?: Display these instructions\n\r")
        self.write_string("z: Zero the encoder position\n\r")
        self.write_string("p: Print out the encoder position\n\r")
        self.write_string("d: Print out the encoder delta\n\r")
        self.write_string("\n\r")

    ## Run the help command
    def run_help(self):
        self.print_instructions()

    ## Run the command to zero the encoder
    def run_zero_encoder(self):
        self.encoder.set_position(0)
    
    ## Run the command to print the encoder position
    def run_pos_encoder(self):
        pos = self.encoder.get_position()
        pos_str = "The encoder position is: "+str(pos)
        self.write_string(pos_str)
        self.write_string("\n\r")

    ## Run the command to print the encoder delta
    def run_delta_encoder(self):
        delta = self.encoder.get_delta()
        delta_str = "The encoder delta is: "+str(delta)
        self.write_string(delta_str)
        self.write_string("\n\r")

    ## Runs when a bad command input was given
    def run_bad_command(self):
        self.write_string("Unknown command! Type ? to see the list of commands\n\r") 

    ## Grabs the user input
    # @return A byte string representing the user input characters
    def get_input(self):
        user_str = self.uart.read()
        return b'' if user_str == None else user_str

    ## Runs a command from the input char
    # @param char A character byte representing the command
    def run_input_command_from_char(self, char):
       if(char == UserInterface.HELP_CHAR):
           self.run_help()
       elif(char == UserInterface.ZERO_CHAR):
           self.run_zero_encoder()
       elif(char == UserInterface.POS_CHAR):
           self.run_pos_encoder()
       elif(char == UserInterface.DELTA_CHAR):
           self.run_delta_encoder()
       else:
           self.run_bad_command()

    ## Runs all the commands from an input byte stream
    # @param input_bytes A byte stream containing all the commands
    def run_input_commands(self, input_bytes):
        for char in [b'%c' % i for i in input_bytes]:
            self.uart.write(char)
            self.write_string("\n\r")
            self.run_input_command_from_char(char)

    ## Runs every cycle, checks for and runs commands using helper functions
    def update(self):
        user_input = self.get_input()
        if(len(user_input) > 0):
            self.run_input_commands(user_input)

