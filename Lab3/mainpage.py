## @file mainpage.py
#  Stores the project information 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Sets up a motor encoder driver and a user interface to interact with the driver
#
#  @section sec_encoder Encoder
#  The encoder uses a timer to convert the encoder channel inputs into a position. Since the board's timer wraps between 0x0000 and 0xFFFF it processes the change in position to maintain an absolute position that does not wrap. Please see Encoder.Encoder for more details.
#
#  @section sec_interface UserInterface
#  The user interface for the user to interact with the encoder. Takes in keyboard commands over the USB serial connection to control the encoder. Can zero the position, print the position, and print the most recent change in position. Please see UserInterface.UserInterface for more details.
#
#  @author Nathan Scheinkman
#
#  @copyright License Info
#
#  @date October 22, 2020
#
