/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Nathan Scheinkman ME305", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab 0", "index.html#sec_lab_0", [
      [ "Documentation", "index.html#subsec_lab_0_doc", null ],
      [ "Source Code", "index.html#subsec_lab_0_src", null ]
    ] ],
    [ "Lab 1", "index.html#sec_lab_1", [
      [ "Documentation", "index.html#subsec_lab_1_doc", null ],
      [ "Source Code", "index.html#subsec_lab_1_src", null ]
    ] ],
    [ "HW 1", "index.html#sec_hw_1", [
      [ "Documentation", "index.html#subsec_hw_1_doc", null ],
      [ "Source Code", "index.html#subsec_hw_1_src", null ]
    ] ],
    [ "Lab 2", "index.html#sec_lab_2", [
      [ "Documentation", "index.html#subsec_lab_2_doc", null ],
      [ "Source Code", "index.html#subsec_lab_2_src", null ]
    ] ],
    [ "Lab 3", "index.html#sec_lab_3", [
      [ "Documentation", "index.html#subsec_lab_3_doc", null ],
      [ "Source Code", "index.html#subsec_lab_3_src", null ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"files.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';