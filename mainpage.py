## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This site holds the documentation for the code submitted to ME305 by Nathan Scheinkman. All the generated documentation can be found below.
#
#  @section sec_lab_0 Lab 0
#  Simple default project to ensure proper functionality of doxygen.
#  @subsection subsec_lab_0_doc Documentation
#  Click here: [Lab 0 Documentation](../Lab0/html/index.html)
#  @subsection subsec_lab_0_src Source Code
#  Click here: [Lab 0 Source Code](https://bitbucket.org/nscheink/me305labs/src/master/Lab0/)
#
#  @section sec_lab_1 Lab 1
#  Generates Fibonacci numbers to an arbitrary size in O(n) time.
#  @subsection subsec_lab_1_doc Documentation
#  Click here: [Lab 1 Documentation](../Lab1/html/index.html)
#  @subsection subsec_lab_1_src Source Code
#  Click here: [Lab 1 Source Code](https://bitbucket.org/nscheink/me305labs/src/master/Lab1/)
#
#  @section sec_hw_1 HW 1
#  Implements a finite-state-machine representing an elevator
#  @subsection subsec_hw_1_doc Documentation
#  Click here: [HW 1 Documentation](../HW1/html/index.html)\n 
#  The documentation includes a diagram representation of the FSM
#  @subsection subsec_hw_1_src Source Code
#  Click here: [HW 1 Source Code](https://bitbucket.org/nscheink/me305labs/src/master/HW/HW1/)
#
#  @section sec_lab_2 Lab 2
#  Blink an LED on a Nucleo board using changing signals
#  @subsection subsec_lab_2_doc Documentation
#  Click here: [Lab 2 Documentation](../Lab2/html/index.html)
#  @subsection subsec_lab_2_src Source Code
#  Click here: [Lab 2 Source Code](https://bitbucket.org/nscheink/me305labs/src/master/Lab2/)
#
#  @section sec_lab_3 Lab 3
#  Interface with a Motor Encoder on a Nucleo board using Timer Channels
#  @subsection subsec_lab_3_doc Documentation
#  Click here: [Lab 3 Documentation](../Lab3/html/index.html)
#  @subsection subsec_lab_3_src Source Code
#  Click here: [Lab 3 Source Code](https://bitbucket.org/nscheink/me305labs/src/master/Lab3/)
#
#  @author Nathan Scheinkman
#
#  @copyright License Info
#
#  @date October 22, 2020
#
